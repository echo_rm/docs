# Internationalization

If you want to make your app available to users in languages other than English, you can localize the user interface. 
UI elements that are defined in the app descriptor can be translated into any of the 
[languages supported](https://confluence.atlassian.com/cloud/languages-744721667.html) by the Atlassian Cloud applications. 
This page describes how to implement these translations.

Note, UI elements that are not defined in the app descriptor, like the contents of pages in an app, 
cannot be translated by this process. You will need to take care of these translations separately, 
using the current user's locale (available in [context parameters](../context-parameters/)).

There are three steps to translating a UI element:

 * Specify a localization key for a UI element, using the [i18n Property bean](../modules/fragment/i18n-property.html).
 * Create a translation file for your desired language (e.g. French), and add the localization key and its translated string.
 * Specify the location of the file in the `translations` property of your [app descriptor](../app-descriptor/).

The following example shows how this works. 
 
## Example

In this example, we will translate the name (i.e. title) of a web item to French.

First, define the web item in the app descriptor, as you normally would:

``` json
{
    "modules": {
        "webItems": [
          {
            "url": "/web-item-example",
            "location": "system.preset.filters",
            "key": "web-item-example",
            "name": {
              "value": "Hello world",
              "i18n": "hello.world"
            }
          }
        ]
    }
}
```

Notice that we have added a i18n key of "hello.world" to the "name" of the web item. 
We can use this to replace the default "Hello world" string with a translated string.

To do this, we need to create a translation file that maps the key to a translated string. 
In this example, we'll create a fr_FR.json file with the following contents:

``` json
{
  "hello.world": "Bonjour le monde"
}
```

Finally, we need to tell the app where the translation file is. 
To do this, we specify the location of the file in the translations property of the app descriptor. 
The location can be relative to the base URL of the app, as shown below, or absolute. 
Absolute URLs can be useful if you want to store your translations on a separate server.

``` json
    "translations": {
        "paths": {
            "fr-FR": "/i18n/fr_FR.json"
        }
    }
```

That's it! Now if a user changes their locale to France, your app will look for the French translations file, 
then load the translated string for the web item's name.

## Translation files

Translations are defined in JSON. The JSON properties map localization keys to their translations, as shown in the example below.

``` json
{
  "key1": "translation of key1",
  "key2": "translation of key2"
}
```

Limitations:
* The maximum size of a translation file is 32 kB
* A translation string cannot be longer than 1kB  

The translation files are downloaded during app installation. 
The installation will fail if any of the files are unavailable at that time.

## Versioning translations

The [auto-upgrade mechanism](/platform/marketplace/upgrading-and-versioning-cloud-apps/) for apps makes it easy to version your translations. 
All you need to do is incorporate the version number into the filename or path for each translation file. 
The example below includes the version number in the path of each translation file:


``` json
{
    "translations": {
        "paths": {
            "en-US": "/i18n/1.1/en_US.json",  
            "fr-FR": "/i18n/1.1/fr-FR.json",
            "de-DE": "/i18n/1.0/de-DE.json"
        }
    }
}
```

It works like this: If you modify a translation file, you will increment the version. 
This means that you will change the path for the file, defined in the app descriptor. 
The change to the app descriptor will trigger an automatic upgrade. 
This makes it easy to roll out the changes to all users.

## Caveats  
 
This feature is not supported for all instances yet. 
For example, it does not work on many [development instances](../getting-started/#set-up-a-development-environment). 
When an app is installed in an instance that doesn't support this feature, 
it will be translated to the language that is currently being used in the instance. 
The app will use this language for all users, regardless of their language preferences. 
This will be fixed by the end of June 2017, when all instances will support this feature. 