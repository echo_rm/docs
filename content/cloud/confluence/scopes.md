---
title: "Scopes"
platform: cloud
product: confcloud
category: devguide
subcategory: blocks
date: "2017-10-19"
---

# Scopes

Scopes allow an app to request a particular level of access to an Atlassian product. 

For example:

* Within a particular product instance an administrator may further limit the actions that an app 
may perform. This is valuable because it allows administrators to safely install apps that they 
otherwise would not.
* The scopes may allow the *potential* to access beta or non-public APIs that are later changed in 
or removed from the Atlassian product. The inclusion of the API endpoint in a scope does not imply 
that the product makes this endpoint public. Read the 
[Confluence API documentation](../about-confluence-cloud-rest-api/) for details.

{{% note %}}Some scopes automatically imply that the app will be granted other scopes; there are 
defined below.{{% /note %}}

The following scopes are available for use by Atlassian Connect Confluence apps:

* `NONE` &ndash; can access app defined data; scope does not need to be declared in the descriptor
* `READ` &ndash; can view, browse, read information from Confluence
* `WRITE` &ndash; can create or edit content in Confluence, but not delete them (implies: `READ`)
* `DELETE` &ndash; can delete entities from Confluence (implies: `READ`, `WRITE`)
* `SPACE_ADMIN` &ndash; can administer a space in Confluence (implies: `READ`, `WRITE`, `DELETE`)
* `ADMIN` &ndash; can administer the entire Confluence instance (implies: `READ`, `WRITE`, `DELETE`, `SPACE_ADMIN`)
* `ACT_AS_USER` &ndash; can enact services on a user's behalf.

### Example

Scopes are declared as a top level attribute of [atlassian-connect.json app descriptor](../app-descriptor/) as in this example:

``` json
    {
        "baseUrl": "http://my-addon.com",
        "key": "atlassian-connect-addon",
        "scopes": [
            "read", "write"
        ],
        "modules": {}
    }
```

## Reference

See [Confluence scopes reference](/cloud/confluence/rest/) for specific resources and related scopes.