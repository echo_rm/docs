# Scopes

Scopes allow an app to request a particular level of access to an Atlassian product.

* Within a particular product instance, an administrator may further limit app actions, allowing administrators to safely install apps they otherwise would not.
* The scopes may allow the *potential* to access beta or non-public APIs that are later changed in or removed from the
Atlassian product. The inclusion of the API endpoint in a scope does not imply that the product makes this endpoint
public. Read the [Jira Cloud platform REST API documentation](/cloud/jira/platform/rest/) for details.

The following scopes are available for use by Atlassian Connect Jira apps:

* `NONE` &ndash; can access app defined data - this scope does not need to be declared in the descriptor.
* `READ` &ndash; can view, browse, read information from Jira
* `WRITE` &ndash; can create or edit content in Jira, but not delete them
* `DELETE` &ndash; can delete entities from Jira
* `PROJECT_ADMIN` &ndash; can administer a project in Jira
* `ADMIN` &ndash; can administer the entire Jira instance
* `ACT_AS_USER` &ndash; can enact services on a user's behalf.



### Example

Scopes are declared as a top level attribute of `atlassian-connect.json` [app descriptor](../app-descriptor/) as in this example:

``` json
    {
        "baseUrl": "http://my-app.com",
        "key": "atlassian-connect-app",
        "scopes": [
            "read", "write"
        ],
        "modules": {}
    }
```
### Reference

See [Jira scopes reference](../jira-rest-api-scopes/) for specific resources and related scopes.