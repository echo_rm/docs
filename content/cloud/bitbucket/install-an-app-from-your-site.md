  ---
title: "Install an app from your site"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: intro 
date: "2016-03-14"
---

# Install an app from your site or application 

One of the big advantages of integrating with Bitbucket Connect is that users
have a simple two-click process for installing your app. The first click is
either on the **Add** button in the Bitbucket app directory, or an **Install**
button embedded on a website, or in an application. The second click approves Bitbucket access permissions for your app and completes the installation. 

### Button placement and trust  

When users click your install button, the next page that appears requests access permissions to their Bitbucket account. For this reason, be careful that you place the install button in a location users will be able to trust such as a marketing site, product information site, or company sponsored blog post. 

### Create your button

The `href` of the button or link must be of the form:  

``` html
https://bitbucket.org/site/addons/authorize?descriptor_uri=<descriptor_uri>&redirect_uri=<redirect_uri>
```

The following rules apply to the URI construction: 

* Both URIs must be absolute.
* The `descriptor_uri` must point to your app's
descriptor. 
* The `redirect_uri` specifies a URL to redirect the user to after
they install the app. 
* The `redirect_uri` must start with the `baseUrl`
of your app. Both are plain URLs (not URI Templates) and do not support
context parameters.

### Create a button using AtlasKit

AtlasKit makes delivering a beautiful button simple. 

![Connect to Bitbucket button](/cloud/bitbucket/images/connect-bb-button.png)

The button above is just a simple link styled with the [AtlasKit reduced UI pack](http://aui-cdn.atlassian.com/atlaskit/registry/reduced-ui-pack/latest/index.html):

``` html
<html>
  <head>
    <link rel="stylesheet" href="https://unpkg.com/@atlaskit/reduced-ui-pack@7.0.0/dist/bundle.css" media="all">
    <link rel="stylesheet" href="https://unpkg.com/@atlaskit/css-reset@1.1.5/dist/bundle.css" media="all">
  </head>
  <body>
    <a href="https://bitbucket.org/site/addons/authorize?descriptor_uri=https://example.com&redirect_uri=https://example.com/welcome">
      <button type="button" class="ak-button ak-button__appearance-primary">Connect to Bitbucket</button>
    </a>
  </body>
</html>
```
## Post install considerations and error states 

If the user accepts the installation, and the app installs successfully, they
will be redirected to the `redirect_uri` with the query parameter
`clientKey` appended. The `clientKey` is an opaque key that references the
app's installation for the user or team that installed the app.

If the user *does not* accept the installation, or the app installation
fails, they will be redirected to the `redirect_uri` with the query parameters
`error` and `error_description` appended. At time of writing the known list of
`error` codes are:

<table class="aui">
   <thead>
          <tr>
              <th>Error</th>
              <th>Description</th>
          </tr>
      </thead>
  <tr>
    <td><code>access_denied</code></td>
    <td>The user has opted not to grant the app access to their account.</td>
  </tr>
  <tr>
    <td><code>invalid_descriptor</code></td>
    <td>The descriptor found at <code>descriptor_uri</code> is invalid.</td>
  </tr>
  <tr>
    <td><code>invalid_request</code></td>
    <td>The install URL or parameters were malformed in some way. Common causes are:
    <ul>
      <li>the <code>descriptor_uri</code> or <code>redirect_uri</code> parameter is missing</li>
      <li>the <code>redirect_uri</code> does not start with the <code>baseUrl</code> specified in the descriptor</li>
      <li>the <code>descriptor_uri</code> is not secured by HTTPS</li>
    </ul>
  </tr>
  <tr>
    <td><code>server_error</code></td>
    <td>Something unexpected went wrong.</td>
  </tr>
</table>

Check the `error_description` parameter passed alongside the `error` parameter
for more information about the specific cause. Your page should be resilient to
additional error codes being added in future.

### JavaScript library option 

Another option for creating your install button is the [Bitbucket install button] JavaScript library, which provides a nice API for handling `redirect_uri` parameters if you prefer not to do the parsing yourself.



[addon-directory]: https://bitbucket.org/account/addon-directory
[settings]: https://bitbucket.org/account/
[Bitbucket install button]: https://bitbucket-install-button.aerobatic.io/
[marketplace]: marketplace.html
[lifecycle]: lifecycle.html
[onboarding]: onboarding.html
