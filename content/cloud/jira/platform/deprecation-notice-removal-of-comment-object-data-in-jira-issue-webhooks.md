---
title: "Deprecation notice - removal of comment object data"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
aliases:
- /jiracloud/41746521.html
- /jiracloud/41746521.md
confluence_id: 41746521
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41746521
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41746521
date: "2017-09-11"
---

# Deprecation notice - removal of comment object data in jira:issue\_\* webhooks

In October 2016, we announced upcoming changes to how `comment` data is sent in webhooks. 

The following webhooks will no longer contain any `comment` objects in their body:

- `jira:issue_created`
- `jira:issue_deleted`
- `jira:issue_updated`

These features will be removed in the future, at least six months after this notice was published, as described in the [Atlassian REST API Policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy). 
Please watch [ACJIRA-1225](https://ecosystem.atlassian.net/browse/ACJIRA-1225) for detailed updates on the status of this change.

## What will happen if I do nothing?

If your app uses the `comment` object data from any registered `jira:issue_*` webhooks, you may encounter complications from the absence of this object after its removal.

## Replacement

These features have been replaced with the `comment_created`, `comment_deleted` and `comment_updated` [webhooks](/cloud/jira/platform/webhooks/). These webhooks are available today.

Report feature requests and bugs for Jira Cloud and webhooks in the [ACJIRA project](https://ecosystem.atlassian.net/projects/ACJIRA) on ecosystem.atlassian.net.