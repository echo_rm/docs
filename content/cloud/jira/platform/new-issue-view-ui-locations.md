---
title: "New issue view UI locations"
platform: cloud
product: jiracloud
category: reference
subcategory: modules
date: "2018-01-18"
---

# New issue view UI locations

{{% note title="Alpha preview" %}}

<br>
The new Jira issue view is currently in alpha for *vendor preview only*. The new modules and extension patterns on this page only relate to Jira Software right now, but they'll be relevant for Jira Service Desk in future. [Learn more about the Alpha preview](/blog/2017/12/alpha-program-for-the-new-jira-issue-view/).

{{% /note %}}

The most common location to add content from your app is on Jira’s *issue view*. In the [*new* Jira issue view](https://confluence.atlassian.com/display/JIRACORECLOUD/The+new+Jira+issue+view), there are two main ways for you to add content: a *quick-add button* to add issue content, and a *glance*. You can also create a view that users can switch to in the activity section of the issue, and include items within the issue actions menu. 

<p style="text-align: center;"><img src="/cloud/jira/platform/images/new-issue-view-locations.png" alt="Issue view locations"></p>

<br>

----

<br>
## Issue content

The [issue content module][1] lets you create an experience where users can add content or media from your app via a quick-add button. The content they add helps to describe the Jira issue. The new Jira issue view has a standard set of quick-add buttons to add attachments, subtasks, and linked issues, and you can add content from your app to this list.

If your app lets users embed a diagram, for example, or link to a design prototype or external document, you should use a quick-add button.

<p style="text-align: center;"><img src="/cloud/jira/platform/images/Anatomy-of-a-quick-add-button.png" alt="Quick-add buttons - annotated" width="400"></p>

{{% tip %}} Check out our [design guidelines](/cloud/jira/platform/issue-view/) for tips on creating a great user experience with your app.{{% /tip %}}

### Properties

Properties for issue content are defined in the documentation for the [issue content][1] module.

### Sample descriptor JSON

``` json 
{
  "modules": {
    "jiraIssueContents": [
      {
        "icon": {
          "width": 24,
          "height": 24,
          "url": "/my_icon.svg"
        },
        "target": {
          "type": "web_panel",
          "url": "/url_to_panel_content_page.htm"
        },
        "tooltip": {
          "value": "This is a tooltip"
        },
        "name": {
          "value": "My Issue Content Panel"
        },
        "key": "my-issue-content-panel"
      }
    ]
  }
}
```

### Quick-add button backwards compatibility

The quick-add pattern is backwards compatible with the `atl.Jira.view.issue.left.context` location, but we strongly recommend you create or update your app using the Atlassian Connect [issue content][1] module to provide a high quality user experience.

<br>

----

<br>
## Glance

*Glances* are special user interface elements that appear in the right sidebar of the issue view under the status, and alongside other fields like assignee, priority, and labels. They provide a quick way for a user to get an overview of some information provided by your app that relates to the issue. A user can interact with a glance to open its detail view to get more information from your app.

<p style="text-align: center;"><img src="/cloud/jira/platform/images/Anatomy-of-a-glance.png" alt="Glance example - annotated" width="400"></p>

{{% tip %}} Check out our [design guidelines](/cloud/jira/platform/issue-view/) for tips on creating a great user experience with your app.{{% /tip %}}

### Properties

Properties for glances are defined in the documentation for the [issue glance][2] module.

### Sample descriptor JSON

``` json
{
  "modules": {
    "jiraIssueGlances": [
      {
        "icon": {
          "width": 24,
          "height": 24,
          "url": "my_icon.svg"
        },
        "content": {
          "type": "label",
          "label": {
            "value": "my label"
          }
        },
        "target": {
          "type": "web_panel",
          "url": "/panel_url"
        },
        "name": {}
      }
    ]
  }
}
```

### Glance backwards compatibility

Glances are backwards compatible with the `atl.Jira.view.issue.right.context` location, but we strongly recommend you create or update your app using the Atlassian Connect [issue glance][2] module to provide a high quality user experience.

<br>

----

<br>
## Issue actions and activity
The new issue view is backwards-compatible with the existing *issue actions* and *issue activity* locations. See [issue view UI locations][4] for more info on issue actions, and the [tab panel module][5] for info on issue activity.

[1]: /cloud/jira/platform/modules/issue-content/
[2]: /cloud/jira/platform/modules/issue-glance/
[3]: /cloud/jira/platform/modules/web-item/
[4]: /cloud/jira/platform/issue-view-ui-locations/
[5]: /cloud/jira/platform/modules/tab-panel/