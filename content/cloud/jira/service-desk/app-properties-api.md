---
title: "App properties API"
platform: cloud
product: jsdcloud
category: reference
subcategory: appapi
date: "2017-10-26"
---
# App properties

Atlassian Connect provides a set of REST APIs specifically designed for use by apps. The `api-name` used is `atlassian-connect`.

<div class="ac-js-methods">
  <dl>
  	<dt>
      <h2 class="name" id="get-addons-addonkey-properties">
		GET .../addons/{addonKey}/properties
      </h2>
    </dt>
    <dd>
      <div class="class-description">
        <p>
          Returns a list of property keys for the given app key.
        </p>
      </div>
      <h4>Parameters</h4>
      <table class="params table table-striped">
        <thead>
          <tr>
            <th>Location</th>
            <th>Name</th>
            <th>Type</th>
            <th class="last">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="location">
              Path
            </td>
            <td class="name">
              <code>addonKey</code>
            </td>
            <td class="type">
              <code>string</code>
            </td>
            <td class="description last">
              <p>The key of the app, as defined in its descriptor</p>
            </td>
          </tr>
        </tbody>
      </table>
      <h4>Response representations</h4>
      <h5><code>200</code> - application/json</h5>
      <div class="notrunnable example-container">
        {{< highlight json >}}
{
  "keys" : [
        {
          "key" : "first_key",
          "self" : "/rest/api/atlassian-connect/${addonKey}/properties/first_key"
        },
        {
          "key" : "another_key",
          "self" : "/rest/api/atlassian-connect/${addonKey}/properties/another_key"
        }
  ]
}
        {{< /highlight >}}
      </div>
      <h5><code>401</code> - application/json</h5>
      <p>Request without credentials or with invalid credentials, e.g. by an uninstalled app.</p>
      <h5><code>404</code> - application/json</h5>
      <p>Request issued by a user with insufficient credentials, e.g. for an app's data by anyone but the app itself,
        or for a app that does not exist.</p>
    </dd>
  </dl>
  <dl>
    <dt>
      <h2 class="name" id="get-addons-addonkey-properties-propertykey">
		GET .../addons/{addonKey}/properties/{propertyKey}
      </h2>
    </dt>
    <dd>
      <div class="class-description">
        <p>
          Returns a property for the given property key.
        </p>
      </div>
      <h4>Parameters</h4>
      <table class="params table table-striped">
        <thead>
          <tr>
            <th>Location</th>
            <th>Name</th>
            <th>Type</th>
            <th class="last">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="location">
              Path
            </td>
            <td class="name">
              <code>addonKey</code>
            </td>
            <td class="type">
              <code>string</code>
            </td>
            <td class="description last">
              <p>The key of the app, as defined in its descriptor</p>
            </td>
          </tr>
          <tr>
            <td class="location">
              Path
            </td>
            <td class="name">
              <code>propertyKey</code>
            </td>
            <td class="type">
              <code>string</code>
            </td>
            <td class="description last">
              <p>The key of the property</p>
            </td>
          </tr>
          <tr>
            <td class="location">
              Query
            </td>
            <td class="name">
              <code>jsonValue</code>
            </td>
            <td class="type">
              <code>boolean</code>
            </td>
            <td class="description last">
              <p>Set to <code>true</code> to have the <code>value</code> field return as a JSON object. If false it will return the <code>value</code> field
                as a string containing escaped JSON. This will be deprecated during June 2016 and the behavior will default
                to true.
              </p>
            </td>
          </tr>
        </tbody>
      </table>
      <h4>Response representations</h4>
      <h5><code>200</code> - application/json</h5>
      <div class="notrunnable example-container">
        {{< highlight json >}}
{
"key" : "abcd",
"value" : true,
"self" : "/rest/api/atlassian-connect/${addonKey}/properties/abcd"
}
        {{< /highlight >}}
      </div>
      <h5><code>400</code> - application/json</h5>
      <p>Property key longer than 127 characters.</p>
      <h5><code>401</code> - application/json</h5>
      <p>Request without credentials or with invalid credentials, e.g. by an uninstalled app.</p>
      <h5><code>404</code> - application/json</h5>
      <p>Request to get a property that does not exist.</p>
      <h5><code>404</code> - application/json</h5>
      <p>Request issued by a user with insufficient credentials, e.g. for an app's data by anyone but the app itself,
        or for a app that does not exist.</p>
    </dd>
  </dl>
  <dl>
    <dt>
      <h3 class="name" id="put-addons-addonkey-properties-propertykey">
		PUT .../addons/{addonKey}/properties/{propertyKey}
      </h3>
    </dt>
    <dd>
      <div class="class-description">
        <p>
          Creates or updates a property.
        </p>
      </div>
      <h4>Parameters</h4>
      <table class="params table table-striped">
        <thead>
          <tr>
            <th>Location</th>
            <th>Name</th>
            <th>Type</th>
            <th class="last">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="location">
              Path
            </td>
            <td class="name">
              <code>addonKey</code>
            </td>
            <td class="type">
              <code>string</code>
            </td>
            <td class="description last">
              <p>The key of the app, as defined in its descriptor</p>
            </td>
          </tr>
          <tr>
            <td class="location">
              Path
            </td>
            <td class="name">
              <code>propertyKey</code>
            </td>
            <td class="type">
              <code>string</code>
            </td>
            <td class="description last">
              <p>The key of the property</p>
            </td>
          </tr>
        </tbody>
      </table>
      <h4>Response representations</h4>
      <h5><code>200</code> - application/json</h5>
      <p>Property updated.</p>
      <h5><code>201</code> - application/json</h5>
      <p>Property created.</p>
      <h5><code>400</code> - application/json</h5>
      <p>Property key longer than 127 characters.</p>
      <h5><code>400</code> - application/json</h5>
      <p>Request made with invalid JSON.</p>
      <h5><code>401</code> - application/json</h5>
      <p>Request without credentials or with invalid credentials, e.g. by an uninstalled app.</p>
      <h5><code>404</code> - application/json</h5>
      <p>Request to get a property that does not exist.</p>
      <h5><code>404</code> - application/json</h5>
      <p>Request issued by a user with insufficient credentials, e.g. for an app's data by anyone but the app itself,
        or for a app that does not exist.</p>
    </dd>
  </dl>
  <dl>
    <dt>
      <h3 class="name" id="delete-addons-addonkey-properties-propertykey">
		DELETE .../addons/{addonKey}/properties/{propertyKey}
      </h3>
    </dt>
    <dd>
      <div class="class-description">
        <p>
          Deletes a property.
        </p>
      </div>
      <h4>Parameters</h4>
      <table class="params table table-striped">
        <thead>
          <tr>
            <th>Location</th>
            <th>Name</th>
            <th>Type</th>
            <th class="last">Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="location">
              Path
            </td>
            <td class="name">
              <code>addonKey</code>
            </td>
            <td class="type">
              <code>string</code>
            </td>
            <td class="description last">
              <p>The key of the app, as defined in its descriptor</p>
            </td>
          </tr>
          <tr>
            <td class="location">
              Path
            </td>
            <td class="name">
              <code>propertyKey</code>
            </td>
            <td class="type">
              <code>string</code>
            </td>
            <td class="description last">
              <p>The key of the property</p>
            </td>
          </tr>
        </tbody>
      </table>
      <h4>Response representations</h4>
      <h5><code>204</code> - application/json</h5>
      <p>Property deleted.</p>
      <h5><code>400</code> - application/json</h5>
      <p>Property key longer than 127 characters.</p>
      <h5><code>401</code> - application/json</h5>
      <p>Request without credentials or with invalid credentials, e.g. by an uninstalled app.</p>
      <h5><code>404</code> - application/json</h5>
      <p>Request to get a property that does not exist.</p>
      <h5><code>404</code> - application/json</h5>
      <p>Request issued by a user with insufficient credentials, e.g. for an app's data by anyone but the app itself,
        or for a app that does not exist.</p>
    </dd>
  </dl>
</div>
