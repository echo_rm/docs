---
aliases:
    - /jiracloud/context-parameters.html
    - /jiracloud/context-parameters.md
    - /cloud/jira/platform/concepts/context-parameters.html
title: Context parameters
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2017-08-24"
---
{{< include path="docs/content/cloud/connect/concepts/jira-context-parameters.snippet.md">}}
