---
title: Showcase Zephyr
logo_url: /img/logo-zephyr-white.png
logo_css_class: zephyr-logo
heading1: "Success Story: Zephyr"
byline: "Zephyr builds a multi-million dollar business in the Atlassian ecosystem"
lede: "Zephyr is a leading provider of agile test management products, with more than 9,000 customers in 100 countries. Based in San Francisco, Zephyr is a top Atlassian Marketplace vendor."
---

## Challenge
  
Zephyr was a growing startup that had developed test management software that integrated into Bugzilla, and was looking for more expansion opportunities. When people started asking for integration with Jira, the company saw huge potential with Atlassian.

## Solution

Zephyr wanted its app to sit inside of Jira to deliver the best functionality to customers. At AtlasCamp, Zephyr developers worked side-by-side with Atlassian to build the integration. Samir Shah, founder at Zephyr said,
"Atlassian was so open about everything, helping us through the process. They asked what we needed and provided APIs and source code. No other company will do that for you."
                                                    
Atlassian's Marketplace provided Zephyr selling and marketing resources to tap into for its Jira app, as well as a massive available market. "No other platform or ecosystem offers this kind of reach," says Shah. "If you build something useful to customers, it will be instantly available to 60,000 customers with a global network of 350 partners selling on your behalf."  

## Benefits

Thanks to Marketplace tools and Atlassian support, Zephyr rapidly developed its first Jira app and got over 100 customers and $100,000 in revenue the first quarter. It grew from 20 employees in 2012 to 90 employees in 2016, and has recently released a Cloud version of their popular apps. It now has almost 10,000 customers in 106 countries.
                                                    
"Atlassian has no sales force, and Zephyr didn't need one either," explains Shah. "With the Atlassian ecosystem, you already have a market of more than half a million instances and a Marketplace to instantly sell to them."

{{% linkblock href="../xpandit/" img="/img/chat-bubble.png" text="Xpand IT" section="server" align="right" %}}
{{% linkblock href="../wittified/" img="/img/clouds.png" text="Wittified" section="cloud" align="left" %}}
