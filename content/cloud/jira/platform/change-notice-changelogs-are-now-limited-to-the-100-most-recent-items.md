---
title: "Changelogs are now limited to the 100 most recent items"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
aliases:
- /jiracloud/change-notice-changelogs-are-now-limited-to-the-100-most-recent-items-41747134.html
- /jiracloud/change-notice-changelogs-are-now-limited-to-the-100-most-recent-items-41747134.md
confluence_id: 41747134
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41747134
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41747134
date: "2017-09-11"
---
# Change Notice - Changelogs are now limited to the 100 most recent items

On **1 April, 2017** we will be limiting the number of items changelog will return.

- The changelog limit will now return the **100 most recent items**.

These features will be removed six months after this notice is published, as described in the [Atlassian REST API Policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

## Replacement

There is no replacement for this change. 

Learn more about [webhooks](/cloud/jira/platform/webhooks/).

Report feature requests and bugs for Jira Cloud and webhooks in the [ACJIRA project](https://ecosystem.atlassian.net/projects/ACJIRA) on ecosystem.atlassian.net.