---
title: "Request"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# Request

## Methods

### request (url, options)

Execute an XMLHttpRequest in the context of the host application.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>url</code></td>
            <td>String</td>
            <td>either the URI to request or an options object (as below) containing at least a <code>url</code> property; should be relative to the context path of the host application</td>
        </tr>
        <tr>
                <td><code>options</code></td>
            <td>RequestProperties</a></span>
            </td>
            <td>a <code>RequestProperties</code> object (see below for properties list)</td>
        </tr>
	</tbody>
</table>

#### Example
        
``` javascript
// Display an alert box with a list of Jira dashboards using the Jira REST API.
AP.require('request', function(request){
  request({
    url: '/assets/js/rest-example.json',
    success: function(responseText){
      alert(responseText);
    }
  });
});
```


## RequestProperties

An object containing the options of a Request.
    
### Properties

<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
        <tr>
                <td><code>url</code></td>
            <td>String</td>
            <td>the URL to request from the host application, relative to the host's context path</td>
        </tr>
        <tr>
                <td><code>type</code></td>
            <td>String</td>
            <td>the HTTP method name; defaults to <code>GET</code></td>
        </tr>
        <tr>
                <td><code>cache</code></td>
            <td>Boolean</td>
            <td>if the request should be cached; default is true</td>
        </tr><tr>
                <td><code>data</code></td>

            <td>String</td>

            <td>the string entity body of the request; required if type is <code>POST</code> or <code>PUT</code></td>
        </tr>
        <tr>
                <td><code>contentType</code></td>
            <td>String</td>
            <td>the content-type string value of the entity body, above; required when data is supplied</td>
        </tr>
        <tr>
                <td><code>headers</code></td>
            <td>Object</td>
            <td>an object containing headers to set; supported headers are <code>Accept</code></td>
        </tr>
        <tr>
                <td><code>success</code></td>
            <td>function</td>
            <td>a callback function executed on a 200 success status code</td>
        </tr>
        <tr>
                <td><code>error</code></td>
            <td>function</td>
            <td>a callback function executed when a HTTP status error code is returned</td>
        </tr>
    </tbody>
</table>

