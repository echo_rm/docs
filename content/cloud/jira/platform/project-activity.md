---
title: "Jira activity "
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
guides: tutorials
date: "2017-10-24"
---

# Jira activity 

## Display your projects in Jira

In this tutorial, you'll learn about:

* [Configuring your development environment](#environment)  
* [Adding an _Activity_ link in the navigation header](#activity-header)  
* [Creating the D3.js table to display your projects](#architect-page)
* [Adding data and verifying your app works](#check)

This tutorial shows you how to build a static Connect app that displays your Jira projects 
in a table, accessible via an _Activity_ link in the header. 

Your app will use the [Jira REST API](https://docs.atlassian.com/jira/REST/latest/) 
to get information about projects in your instance. You'll use the <a href="http://nodejs.org" target="_blank">Node.js</a> 
framework and <a href="https://bitbucket.org/atlassian/atlassian-connect-express/" target="_blank">
Atlassian Connect Express (ACE)</a> to interface with Jira. Finally, you'll create a table of your 
projects using <a href="http://d3js.org/" target="_blank">D3.js</a>.

When you're finished, your app will look similar to this: 

![Jira activity completed](/cloud/connect/images/jira-activity-4.png)

## Configuring your development environment  

In this step, you'll confirm you have Node.js installed, and install the 
[Atlassian Connect Express (ACE)](https://bitbucket.org/atlassian/atlassian-connect-express/) toolkit. 
ACE helps you create Connect apps using Node.js. 
It also detects changes made to your [`atlassian-connect.json` descriptor](../modules/) 
file, so you don't need to continually restart your app as you develop. 
Importantly, ACE also handles [JSON web token (JWT)](../concepts/understanding-jwt.html), 
so that requests between your app and the Jira application are signed and authenticated. 

1. Install <a href="http://www.nodejs.org/" target="_blank">Node.js</a> 4.5.0 or later.
	If you use <a href="http://brew.sh/" target="_blank">Homebrew</a>, you can use the following command:
	
	``` bash
	brew install node
	```
	If you use Windows or don't use Homebrew, you can <a href="http://nodejs.org/download/" target="_blank">
	download and install Node.js directly</a>.
	You might need to enter `sudo`.  
1. Install <a href="https://bitbucket.org/atlassian/atlassian-connect-express/" target="_blank">ACE</a> using the 
	<a href="https://www.npmjs.org/package/npm-install" target="_blank"><tt>npm install</tt> command</a>.
	
	``` bash
	npm install -g atlas-connect
	```
1. Create a new ACE project called `jira-activity`.
	
	``` bash
	atlas-connect new -t jira jira-activity
	```
1. Change to your new `jira-activity` directory.
	
	``` bash
	cd jira-activity/
	```
1. Install Node.js dependencies for your `jira-activity` project.
	
	``` bash
	npm install
	```
1. Get a Jira development environment by following the [Development Setup](../getting-started/#set-up-a-development-environment)


## Install your app and add an Activity link

Now you've got the basic architecture for your app. If you open your new `jira-activity` directory, 
you'll see essentials like the [`atlassian-connect.json` descriptor](../modules/) in the 
root. You'll also see an `app.js` file. 

In this step, you'll prune some of the stub code, and install your app in Jira using ACE.

1. Open the [`atlassian-connect.json` descriptor](../modules/) file in your favorite editor or IDE.
1. Replace the `key`, `name`, `description`, and `vendor` name and URL with these fields: 
	
	``` json
	{
	    "key": "jira-activity",
	    "name": "Jira Project Activity",
	    "description": "A Connect app that displays Jira projects in a table",
	    "vendor": {
	        "name": "Atlassian Developer Relations",
	        "url": "https://developer.atlassian.com/"
		},
	```
	This names your app in in your Jira instance, and essentially makes it yours. 
1. Replace the content of `modules` with a [`generalPages` module](/cloud/jira/platform/modules/page.html):
	
	``` json
     "generalPages": [
 
	    {
	        "key": "activity",
	        "location": "system.top.navigation.bar",
	        "name": {
	            "value": "Activity"
	        },
	        "url": "/activity",
	        "conditions": [{
	            "condition": "user_is_logged_in"
	        }]
	    }
    ]
   ```
   
This adds an _Activity_ link in the Jira header (`system.top.navigation.bar`), and sets a URL for your app 
to use under `/activity`. It also provides a condition so that the REST API shows private projects visible to 
a logged in user, not just public projects. 

At this point, your descriptor file should look like this:

``` json
{
    "key": "jira-activity",
    "name": "Jira Project Activity",
    "description": "A Connect app that displays Jira projects in a table",
    "vendor": {
        "name": "Atlassian Developer Relations",
        "url": "https://developer.atlassian.com/"
    },
    "baseUrl": "{{localBaseUrl}}",
    "links": {
        "self": "{{localBaseUrl}}/atlassian-connect.json",
        "homepage": "{{localBaseUrl}}/atlassian-connect.json"
    },
    "authentication": {
        "type": "jwt"
    },
    "lifecycle": {
        // atlassian-connect-express expects this route to be configured to manage the installation handshake
        "installed": "/installed"
    },
    "scopes": [
        "READ"
    ],
     "modules": {
         "generalPages": [
             {
                 "key": "activity",
                 "location": "system.top.navigation.bar",
                 "name": {
                     "value": "Activity"
                 },
                 "url": "/activity",
                 "conditions": [{
                     "condition": "user_is_logged_in"
                 }]
             }
         ]
     }
}
```

1. Open a new terminal window.  
1. From your `jira-activity` root, start up a Node.js server:  
	
	``` bash
	$ node app.js
	```
	This starts up your app on a server locally.
1. Make your app available on the public internet, see [Developing locally](../developing/developing-locally-ngrok.html)
1. Install the app in your development Jira instance.
1. Refresh Jira in your browser.   
	You'll see the _Activity_ label in the header: 
	![Jira activity](/cloud/connect/images/jira-activity-1.png)
	This link doesn't go anywhere - yet. You'll fix this in future steps.
1. Back in your editor, open `routes/index.js`.  
	From here, you'll add the `/activity` route to your app.
1. After the `/hello-world` stub code, add:  
	
	``` javascript
	app.get('/activity', addon.authenticate(), function(req, res) {
	    res.render('activity', { title: "Jira activity" });
	});
	``` 
	Your `routes/index.js` file should resemble this:  
	
	``` javascript
	module.exports = function (app, addon) {

	    // Root route. This route will serve the `atlassian-connect.json` unless the
	    // documentation url inside `atlassian-connect.json` is set
	    app.get('/', function (req, res) {
	        res.format({
	            // If the request content-type is text-html, it will decide which to serve up
	            'text/html': function () {
	                res.redirect('/atlassian-connect.json');
	            },
	            // This logic is here to make sure that the `atlassian-connect.json` is always
	            // served up when requested by the host
	            'application/json': function () {
	                res.redirect('/atlassian-connect.json');
	            }
	        });
	    });

	    // The following is stub code for a Hello World app provided by ACE.
	    // You can remove this section since it's not used in this tutorial, 
	    // or leave it here - it makes no difference to this app.

	    // This is an example route that's used by the default "generalPage" module.
	    // Verify that the incoming request is authenticated with Atlassian Connect
	    app.get('/hello-world', addon.authenticate(), function (req, res) {
	            // Rendering a template is easy; the `render()` method takes two params: name of template
	            // and a json object to pass the context in
	            res.render('hello-world', {
	                title: 'Atlassian Connect'
	                //issueId: req.query('issueId')
	            });
	        }
	    );

	    // Add any additional route handlers you need for views or REST resources here...
	    app.get('/activity', addon.authenticate(), function(req, res) {
	        res.render('activity', { title: "Jira activity" });
	    });
	};
	```
	This route titles your __Activity__ page "Jira activity", and ensures that your app 
	is authenticated.  
1. Close and save your `atlassian-connect.json` and `routes/index.js` files.  


## Build the static Activity page  

You've added a link in the Jira header, and now you'll define how your page should look. 
In this step, you'll add the capability for your app to use D3.js, and style 
the page using [Atlassian User Interface (AUI)](https://docs.atlassian.com/aui/latest/index.html). 

1. Open `views/layout.hbs`.
1. Add the following to the `views/layout.hbs` file, near the closing `</body>` tag (following the `</section>`
	line):
	
	``` html
<script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script src="{{furl '/js/jira-activity.js'}}"></script>
	```
	This lets you to use D3.js for your chart. This file is a shared container of styles (like [AUI](https://docs.atlassian.com/aui/latest/index.html))
	and scripts you can use for all pages of your app.    
1. Create a new file called `views/activity.hbs`.   
	This file is a template you'll use to render the `/activity` URL.  
1. Add the following content:
	
	``` html
	{{!< layout}}
	<header class="aui-page-header">
	    <div class="aui-page-header-inner">
	        <div class="aui-page-header-main intro-header">
	            <h1>{{title}}</h1>
	        </div>
	    </div>
	</header>

	<div class="aui-page-panel main-panel">
	    <div class="aui-page-panel-inner">
	        <section class="aui-page-panel-item">
	            <div class="aui-group">
	                <div class="aui-item">
	                    <div class="projects">
	                    </div>
	                </div>
	            </div>
	        </section>
	    </div>
	</div>
	```
1. Create a file called `public/js/jira-activity.js`.  
	This file will take a list of your projects, and generate an HTML table using D3.js.  
1. Add the following content:
    
    ``` javascript
    // Canned functionality for Jira Activity
    $(function() {
        "use strict";
    
        // Get parameters from query string
        // and stick them in an object
        function getQueryParams(qs) {
            qs = qs.split("+").join(" ");
    
            var params = {}, tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;
    
            while (tokens = re.exec(qs)) {
                params[decodeURIComponent(tokens[1])] =
                    decodeURIComponent(tokens[2]);
            }
    
            return params;
        }
    
        AP.define('JiraActivity', {
            buildProjectTable: function(projects, selector) {
    
                var params = getQueryParams(document.location.search);
                var baseUrl = params.xdm_e + params.cp;
    
                function buildTableAndReturnTbody(hostElement) {
                    var projTable = hostElement.append('table')
                        .classed({'project': true, 'aui': true});
    
                    // table > thead > tr, as needed below
                    var projHeadRow = projTable.append("thead").append("tr");
                    // Empty header
                    projHeadRow.append("th");
                    // Now for the next column
                    projHeadRow.append("th").text("Key");
                    projHeadRow.append("th").text("Name");
    
                    return projTable.append("tbody");
                }
    
                var projectBaseUrl = baseUrl + "/browse/";
    
                var rootElement = d3.select(selector);
                var projBody = buildTableAndReturnTbody(rootElement);
    
                // For each data item in projects
                var row = projBody.selectAll("tr")
                    .data(projects)
                    .enter()
                    .append("tr");
    
                // Add a td for the avatar, stick a span in it
                row.append("td").append('span')
                    // Set the css classes for this element
                    .classed({'aui-avatar': true, 'aui-avatar-xsmall': true})
                    .append('span')
                    .classed({'aui-avatar-inner': true})
                    .append('img')
                    // Set the atribute for the img element inside this td > span > span
                    .attr('src', function(item) { return item.avatarUrls["16x16"] });
    
                // Add a td for the project key
                row.append("td").append('span')
                    .classed({'project-key': true, 'aui-label': true})
                    // set the content of the element to be some text
                    .text(function(item) { return item.key; });
    
                // And finally, a td for the project name & link
                row.append("td").append('span')
                    .classed({'project-name': true})
                    .append("a")
                    // make the name a link to the project
                    .attr('href', function(item) { return projectBaseUrl + item.key; })
                    // since we're in the iframe, we need to set _top
                    .attr('target', "_top")
                    .text(function(item) { return item.name; });
            }
        });
    });
    ```
1. Open `public/js/addon.js`. 
	This file uses the [Jira REST API](https://docs.atlassian.com/jira/REST/latest/) to request project information you use to generate the table.   
1. Add the following content:  
    
    ``` javascript
/* app script */
// Myapp functionality
$(function() {
    // Call REST API via the iframe
    // Bridge functionality
    // JiraActivity is registered by an external script that was included
    AP.require(['request', 'JiraActivity'], function(request, JiraActivity) {
        request({
            url: '/rest/api/2/project',
            success: function(response) {
                // Convert the string response to JSON
                response = JSON.parse(response);

                // Call your helper function to build the
                // table, now that you have the data
                JiraActivity.buildProjectTable(response, ".projects");
            },
            error: function(response) {
                console.log("Error loading API (" + uri + ")");
                console.log(arguments);
            },
            contentType: "application/json"
        });
    });
});
	```
	`AP.require` calls the [Connect API](../javascript/module-AP.html), and `url` is the REST API you call (`/rest/api/2/project`). 
	Your Jira instance returns a string response, and `success` converts it to JSON.

1. Save and close all files. 
1. Restart the Node.js app. 
	Shut down the app with __CTRL+C__ and re-run the __`node app.js`__ 
	command.  
1. Click __Activity__ in the header.  
	You'll see an empty page with your "Jira activity" title:  
	![Jira activity](/cloud/connect/images/jira-activity-2.png)
	Your page is blank since your Jira instance doesn't yet have any data, but you'll
	fix that in the next step!  

## Add some data, and verify your app works

Your app is essentially done, but you don't have any data to validate
that your table works. In this step, you'll manually add a few projects, and validate 
that your table reflects the changes.  

1. Click __Projects__ > __Create Project__ in the header.  
	Run through the prompts and create a project.  
1. Repeat as desired. 
	The more data you create, the more your app displays.  
1. Check your app between adding data.  
	You should see your __Activity__ table update each time you click the 
	link.  
	Here's an example what you'll see, using example projects:  
	![Jira activity](/cloud/connect/images/jira-activity-3.png)

## Additional resources

Thanks for trying the tutorial! If you'd like to check your work, feel free to check out the repository here: 

[https://bitbucket.org/atlassianlabs/connect-jira-activity/overview](https://bitbucket.org/atlassianlabs/connect-jira-activity/overview)  

Additionally, we're always interested in making our docs and developer experiences better. If you have 
feedback about this tutorial or other Connect documentation, [let us know](/cloud/jira/platform/get-help/).