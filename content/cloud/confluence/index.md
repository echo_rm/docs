---
title: "Latest updates"
platform: cloud
product: confcloud
category: devguide
subcategory: index
date: "2017-06-23"
---

# Latest updates

We deploy updates to Confluence Cloud frequently. As a Confluence developer, it's important that 
you're aware of the changes. The resources below will help you keep track of what's happening.

## Recent announcements

Confluence Cloud has a new look and feel, including updated navigation. 
[Learn more about the new navigation](https://confluence.atlassian.com/confcloud/new-navigation-and-a-new-look-for-confluence-881536766.html).

## Atlassian Developer blog

Major changes that affect Confluence Cloud developers are announced in the 
[Atlassian Developer blog](https://developer.atlassian.com/blog/categories/confluence/), like new 
Confluence modules or the deprecation of API end points. You'll also find handy tips and articles 
related to Confluence development.

## Product updates

Major changes that affect all users of Confluence Cloud are announced in the [What's new blog](https://confluence.atlassian.com/cloud/what-s-new-686863216.html#What'sNew-confluence) for Atlassian Cloud. This includes new features, bug fixes, and other changes. 