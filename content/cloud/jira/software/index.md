---
title: "Jira Software Cloud API introduction"
platform: cloud
product: jswcloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/jira-software-cloud-development-39981104.html
- /jiracloud/jira-software-cloud-development-39981104.md
confluence_id: 39981104
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39981104
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39981104
date: "2017-09-11"
---

# Integrating with Jira Software Cloud

![Jira cloud integration graphic](/cloud/jira/platform/images/jira-integration.png)

Welcome to Jira Software Cloud development! This overview will cover everything you need to know to integrate with Jira Software Cloud. This includes the Atlassian Connect framework, which is used to integrate with Atlassian Cloud applications, as well as Jira features and services that you can use when building an app.

{{% tip title="Hello world"%}}If you already know the theory and want to jump straight into development, read our [Getting started guide](/cloud/jira/software/getting-started) to build your first Jira Cloud app.{{% /tip %}}

{{< include path="docs/content/cloud/jira/platform/jira-cloud-api-introduction.snippet.md">}}

## Looking for inspiration? <a name="inspiration"></a>

If you are looking for ideas on building the next Jira Software Cloud integration, here are a few examples of what you can build on top of Jira Software Cloud:

-   [Retrospective Tools for Jira] -- This app provides a number of tools for reviewing your project history, and with the Jira Software API they're able to let you filter by board, or even overlay your swimlanes.
-   [Epic Sum Up] -- This app adds a panel to the issue view that allows you to review your epic progress or every issue within the epic.
-   [Tempo Planner for Jira] -- This app fetches information from boards, epics, and backlogs using the Jira Software API, and lets you plan your work by team member.

## More information

-   [Jira Software Cloud tutorials] -- Learn more about Jira Software development by trying one of our hands-on tutorials.
-   [The Atlassian Developer Community](https://community.developer.atlassian.com/t/welcome-to-the-community/84) -- Join the discussion on Jira Software development.

  [product overview]: https://www.atlassian.com/software/jira
  [Getting started guide]: /cloud/jira/software/getting-started
  [Jira Software Cloud REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [Jira Cloud platform REST API]: /cloud/jira/platform/rest/
  [Webhooks]: /cloud/jira/software/webhooks
  [About Jira modules]: /cloud/jira/software/about-jira-modules  
  [Jira Cloud platform documentation]: /cloud/jira/platform/integrating-with-jira-cloud
  [Retrospective Tools for Jira]: https://marketplace.atlassian.com/plugins/com.sngtec.jira.cloud.kanbanalytics/cloud/overview
  [Epic Sum Up]: https://marketplace.atlassian.com/plugins/aptis.plugins.epicSumUp/cloud/overview
  [Tempo Planner for Jira]: https://marketplace.atlassian.com/plugins/com.tempoplugin.tempo-planner/cloud/overview
  [Jira Software Cloud tutorials]: /cloud/jira/software/tutorials-and-guides/
  [jira-agile-development tag]: https://answers.atlassian.com/questions/topics/753774/jira-agile-development
