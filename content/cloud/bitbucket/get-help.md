---
title: "Get help"
platform: cloud
product: bitbucketcloud
category: help
subcategory: help
date: "2018-02-27"
layout: get-help
---

If you are looking for status updates for Atlassian products, no need to file a ticket. Just check out the latest notifications and status in the links below.
